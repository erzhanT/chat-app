import React from 'react';

const GetPost = (props) => {

    return (
        <div>
            <div className="post">
                <h5>Пользователь: {props.post.author} <span>Время: {props.post.datetime}</span></h5>
                <p>Сообщение: {props.post.message}</p>
            </div>
        </div>
    );
};

export default GetPost;