import React from 'react';

const SendPost = (props) => {
    return (
        <div>
            <div>
                <h5>Author:</h5>
                <input
                    type="text"
                    name={'author'}
                    value={props.propsedState.author}
                    className="input-author"
                    required
                    onChange={(e) => props.onChangeHandler(e)}/>
            </div>
            <div>
                <h5>Message:</h5>
                <input
                    type="text"
                    name={'message'}
                    value={props.propsedState.message}
                    className="input-message"
                    required
                    onChange={(e) => props.onChangeHandler(e)}/>
            </div>
            <button onClick={props.sendMessage}>Send</button>
        </div>
    );
};

export default SendPost;