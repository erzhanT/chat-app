import React, {useState, useEffect} from 'react';
import './App.css';
import axios from 'axios';
import GetPost from "../../components/GetPost/GetPost";
import SendPost from "../../components/SendPost/SendPost";


const App = () => {

    const [state, setState] = useState({author: '', message: ''});
    const [posts, setPosts] = useState([]);
    const [lastDate, setLastDate] = useState('');
    const url = 'http://146.185.154.90:8000/messages';

    let interval = null;

    useEffect(() => {
        loadMessage();
    }, []);

    useEffect(() => {
        if (lastDate) {
            interval = setInterval(() => {
                findNewMessage();
            }, 2000)
        }
        return () => clearInterval(interval);
    }, [lastDate])

    const onChangeHandler = (e) => {
        const stateCopy = {...state};
        stateCopy[e.target.name] = e.target.value;
        setState(stateCopy);
    };

    const findNewMessage = () => {
        const last = url + `?datetime=${lastDate}`;

        try {
            axios.get(last).then(result => {
                if(result.data.length > 0) {
                    const postsCopy = [...posts];
                    const newArray = postsCopy.concat(result.data);
                    setLastDate(result.data[result.data.length - 1].datetime);
                    setPosts(newArray)
                }
            })
        } catch (e) {
            console.log(e);
        }

    };

    const loadMessage = async () => {
        try {
            axios.get(url).then(result => {
                setPosts(result.data);
                setLastDate(result.data[result.data.length - 1].datetime)
            })
        } catch (e) {
            console.log(e)
        }

    };

    const sendMessage = async () => {
        try {
            const data = new URLSearchParams();
            data.set('message', state.message);
            data.set('author', state.author);
            await axios.post(url, data);
        } catch (e) {
            console.log(e);
        }
    };

    return (
        <div className="App">
            <SendPost propsedState={state} sendMessage={sendMessage} onChangeHandler={onChangeHandler}/>
            {posts.map((post, index) => (
              <GetPost
                  key={index}
                  post={post}
              />
            ))}
        </div>
    );

};

export default App;
